from rdflib.namespace import Namespace

REG = Namespace('http://purl.org/linked-data/registry#')
UI = Namespace('http://purl.org/linked-data/registry-ui#')
SKOS_EXT = Namespace('http://linked.data.gov.au/def/skos-ext#')
SOSA = Namespace('http://www.w3.org/ns/sosa/')