from rdflib import Graph
from rdflib.namespace import DC


def bind_curies(g: Graph):
    g.bind('dct', 'http://purl.org/dc/terms/')
    g.bind('ldp', 'http://www.w3.org/ns/ldp#')
    g.bind('owl', 'http://www.w3.org/2002/07/owl#')
    g.bind('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    g.bind('rdfs', 'http://www.w3.org/2000/01/rdf-schema#')
    g.bind('reg', 'http://purl.org/linked-data/registry#')
    g.bind('skos', 'http://www.w3.org/2004/02/skos/core#')
    g.bind('ui', 'http://purl.org/linked-data/registry-ui#')
    g.bind('xsd', 'http://www.w3.org/2001/XMLSchema#')
    g.bind('sosa', 'http://www.w3.org/ns/sosa/')
    g.bind('dc', DC)