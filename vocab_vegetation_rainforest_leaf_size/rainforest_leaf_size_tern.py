import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies


def create_content(df):
    g = Graph()

    bind_curies(g)

    collection_uri = 'http://linked.data.gov.au/def/tern-cv/10c3ecd2-1573-4abb-afe5-0315a919b38c'

    g.add((URIRef(collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(collection_uri), DCTERMS.source, Literal('Page 111 to 113 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'Leaf size classes for classifying wet tropical and subtropical rainforests are based on the sizes of the leaves of the tallest stratum trees. Precise calculation of leaf area is not required.'
    g.add((URIRef(collection_uri), SKOS.definition, Literal(definition)))

    for i, row in df.iterrows():
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[4]

        g.add((URIRef(collection_uri), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[2])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(row[0])))

    g.serialize('rainforest-leaf-size-tern.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('rainforest-leaf-size.xlsx')

    create_content(df)