import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies


def create_content(df):
    g = Graph()

    bind_curies(g)

    collection_uri = 'http://linked.data.gov.au/def/tern-cv/0cbd375c-63d2-4ad2-b9df-51e83c90be5a'

    g.add((URIRef(collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(collection_uri), DCTERMS.source, Literal('Page 114 to 115 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = """Many of the simple rainforests and some of the complex and simple–complex rainforests develop strata dominated by particular growth forms. These growth forms are illustrated in Webb et al. (1976). Record the growth form name or code as follows.

These terms are inserted before or within the structural formation class: for example, ‘tall sparse fern forest’; ‘very tall closed fan palm forest’; ‘low closed vine shrubland’; ‘tall closed feather palm forest’."""
    g.add((URIRef(collection_uri), SKOS.definition, Literal(definition)))

    for i, row in df.iterrows():
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[3]

        g.add((URIRef(collection_uri), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[2])))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(row[0])))

    g.serialize('rainforest-indicator-growth-form-tern.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('rainforest-indicator-growth-form.xlsx')

    create_content(df)