# Australian Soil and Land Survey Field Handbook Vocabularies
The handbook (yellow book) consists of classification schemes to describe a standard protocol in surveying vegetation plots. This repository contains the classification schemes from the handbook as Excel spreadsheets and are converted into RDF based on the SKOS model. 

The RDF data is handed over to Simon Cox at CSIRO to be hosted online at http://registry.it.csiro.au/def/soil/au/asls.


## Codebase Structure
Each vocabulary transformation is performed inside a directory prefixed with `vocab_`. The structure of the directory is vocab_<chapter>_<observable-property>. E.g. for growth form in the vegetation chapter, the directory name is constructed as **vocab_vegetation_growth_form**. Each directory contains a Python script to transform the Excel spreadsheet into RDF.  


## Vocabularies

### Vegetation - growth form
| Growth form                                                                    | Code                                       | Definition                                            | Height range (m)               | Life form                      |
|--------------------------------------------------------------------------------|--------------------------------------------|-------------------------------------------------------|--------------------------------|--------------------------------|
| Recorded with rdfs:label, skos:prefLabel and local name for cryptogam and food | Recorded with skos:notation and local name | Recorded with skos:definition and dcterms:description | Recorded with skos:hiddenLabel | Recorded with skos:hiddenLabel |

There are no red codes for **Cryptogam** and **Food**. For now, I have used the labels to construct the URI. The code for **Aquatic higher plants** is also unusual with the value **a1.0 (or w)**. Since this is used to construct the URI, I have URL-encoded it, though it looks very bad. See <growth-form-a1.0+%28or+w%29>.

We would also like to capture the height range information from Table 20, page 95 of the YB. It's currently captured as an additional `skos:hiddenLabel`.

Life form information for each growth form is found on page 101.

### Vegetation - cover class
| Code                                                   | Criteria assessed in field                               | Description                                             | Crown separation ratio                                             | Crown cover %                                           | Foliage cover %                                           |
|--------------------------------------------------------|----------------------------------------------------------|---------------------------------------------------------|--------------------------------------------------------------------|---------------------------------------------------------|-----------------------------------------------------------|
| Recorded with skos:notation on the crown class concept | Recorded with skos:definition on the crown class concept | Recorded with skos:prefLabel on the crown class concept | Recorded with skos:prefLabel on the crown separation ratio concept | Recorded with skos:prefLabel on the crown cover concept | Recorded with skos:prefLabel on the foliage cover concept |

The crown separation ratio concept, crown cover concept and foliage cover concept per row are related using the `skos:related` property. Each of these concepts also have a `skos:narrower` relationship to the cover class concept.  

Need to double check if the crown cover % concepts make sense. There are two terms that are both **<25%**. Same issue exists for foliage cover as well.


### Vegetation - height class
| Height class code                                                           | Height (m)                   | Life form w (woody plants)  | Life form nw (non-woody plants) |
|-----------------------------------------------------------------------------|------------------------------|-----------------------------|---------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel | Recorded with skos:altLabel | Recorded with skos:altLabel     |

Currently the life form name for woody and non-woody plants are recorded with `skos:altLabel`. This current strategy doesn't allow us to identify which alternative label is being used for a woody plant or a non-woody plant and may need to be adjusted in the future. Should the height classes be narrower concepts to the life form concepts?


### Vegetation - life form
| Code                                                                        | Life form                                        |
|-----------------------------------------------------------------------------|--------------------------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel and skos:definition |


### Vegetation - cover-abundance scale
| Code                                                                        | Description                  | Crown cover percentage        |
|-----------------------------------------------------------------------------|------------------------------|-------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel | Recorded with skos:definition |


### Vegetation - rainforest complexity
| Code                                                                        | Complexity                   | Description                   |
|-----------------------------------------------------------------------------|------------------------------|-------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel | Recorded with skos:definition |

*Complexity* is a sub-section of Rainforest, which is a sub-section of Vegetation. I understand some things may be represented as nested collections but I am not sure if this should be or not, or if calling this **rainforest complexity** is appropriate. Complexity is also one of the variables required when surveying a wet tropical or subtropical rainforest (see Table 23).

The handbook uses dot points to describe each of the concepts in rainforest complexity. Since we are just using text, I have changed the dot points to a dash. I understand we should try to preserve the original text but I was not sure what character to use for the dot point.  


### Vegetation - rainforest leaf size
| Code                                                                        | Term describing leaf size of forest stand | Number of individual trees (maximum 10) with specified leaf sizes | Percentage of individuals in tallest stratum with specified leaf size |
|-----------------------------------------------------------------------------|-------------------------------------------|-------------------------------------------------------------------|-----------------------------------------------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel              | Recorded with skos:definition                                     | Recorded with skos:definition                                         |

We need to review how we are capturing the text in the last 2 columns. 


### Vegetation - rainforest indicator growth form
| Code                                                                        | Indicator growth form        | Definition                    |
|-----------------------------------------------------------------------------|------------------------------|-------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel | Recorded with skos:definition |


### Vegetation - rainforest species
| Code                                                                        | Classification               | Definition                    |
|-----------------------------------------------------------------------------|------------------------------|-------------------------------|
| Recorded with skos:notation and used to construct the local name of the URI | Recorded with skos:prefLabel | Recorded with skos:definition |


## Vocabularies not converted
The codes for **Aquatic and wetland types** were not transformed as they are not currently relevant to the datasets which we are working with. 

If we were to vocabularise these codes, we would create a top-level collection "Aquatic and wetland types" with sub-collections:

- A - Marine and coastal zone wetlands
- B - Inland wetlands
- C - Human-made wetlands 


## Contact
**Edmond Chuc**  
*Software engineer*  
e.chuc@uq.edu.au  