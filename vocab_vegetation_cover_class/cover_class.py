import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies
from rdf_utils.namespace import REG, UI, SKOS_EXT, SOSA

import datetime


def create_register_rdf():
    g = Graph()

    bind_curies(g)

    g.add((URIRef('vegetation'), RDF.type, REG.Register))
    g.add((URIRef('vegetation'), RDFS.label, Literal('Vegetation')))
    g.add((URIRef('vegetation'), DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3884-3420')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3903-254X')))
    g.add((URIRef('vegetation'), DCTERMS.created, Literal('2020-06-19', datatype=XSD.date)))
    g.add((URIRef('vegetation'), DCTERMS.description, Literal("""<p>This register contains a machine-readable representation of the classifiers described in chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
<p>This chapter identifies which vegetation attributes should be measured and recorded at field sites in order to describe and classify Australian vegetation.7 The attributes chosen – as well as the required level of detail – depend on the purpose of the survey, which needs to be explicitly recorded.</p>
<p>The data was converted from the print representation to this linked-data form by <a href='https://orcid.org/0000-0002-6047-9864'>Edmond Chuc</a> assisted by <a href='https://orcid.org/0000-0002-3884-3420'>Simon J D Cox</a>.""", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.isFormatOf, Literal("<p>Chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a></p>", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.license, URIRef('http://creativecommons.org/licences/by/4.0')))
    g.add((URIRef('vegetation'), DCTERMS.modified, Literal(datetime.date.today())))
    g.add((URIRef('vegetation'), DCTERMS.publisher, URIRef('http://www.publish.csiro.au/')))
    g.add((URIRef('vegetation'), DCTERMS.rights, Literal('copyright CSIRO 2009, 2020. All rights reserved.')))
    g.add((URIRef('vegetation'), DCTERMS.source, Literal("National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)")))
    g.add((URIRef('vegetation'), REG.notation, Literal('vegetation')))
    g.add((URIRef('vegetation'), UI.hierarchyChildProperty, SKOS.member))
    g.add((URIRef('vegetation'), UI.hierarchyRootProperty, SKOS_EXT.topMemberOf))
    g.add((URIRef('vegetation'), RDFS.seeAlso, URIRef('http://www.publish.csiro.au/nid/22/pid/5230.htm')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(DC)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef('http://purl.org/linked-data/registry')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(UI)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(SOSA)))

    g.serialize('cover-class-register.ttl', format='turtle')


def create_content(df):
    g = Graph()

    bind_curies(g)

    # Cover class collection
    g.add((URIRef('cover-class'), RDF.type, SKOS.Collection))
    g.add((URIRef('cover-class'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('cover-class'), SKOS.prefLabel, Literal('Crown class')))
    g.add((URIRef('cover-class'), DCTERMS.source, Literal('Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef('cover-class'), SKOS.definition, Literal(definition)))
    g.add((URIRef('cover-class'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('cover-class'), UI.topMemberOf, URIRef('')))

    # Crown separation ratio collection
    g.add((URIRef('crown-separation-ratio'), RDF.type, SKOS.Collection))
    g.add((URIRef('crown-separation-ratio'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('crown-separation-ratio'), SKOS.prefLabel, Literal('Crown separation ratio')))
    g.add((URIRef('crown-separation-ratio'), DCTERMS.source, Literal(
        'Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef('crown-separation-ratio'), SKOS.definition, Literal(definition)))
    g.add((URIRef('crown-separation-ratio'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('crown-separation-ratio'), UI.topMemberOf, URIRef('')))

    # Crown cover collection
    g.add((URIRef('crown-cover'), RDF.type, SKOS.Collection))
    g.add((URIRef('crown-cover'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('crown-cover'), SKOS.prefLabel, Literal('Crown cover')))
    g.add((URIRef('crown-cover'), DCTERMS.source, Literal(
        'Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef('crown-cover'), SKOS.definition, Literal(definition)))
    g.add((URIRef('crown-cover'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('crown-cover'), UI.topMemberOf, URIRef('')))

    # Foliage cover collection
    g.add((URIRef('foliage-cover'), RDF.type, SKOS.Collection))
    g.add((URIRef('foliage-cover'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('foliage-cover'), SKOS.prefLabel, Literal('Foliage cover')))
    g.add((URIRef('foliage-cover'), DCTERMS.source, Literal(
        'Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef('foliage-cover'), SKOS.definition, Literal(definition)))
    g.add((URIRef('foliage-cover'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('foliage-cover'), UI.topMemberOf, URIRef('')))

    for i, row in df.iterrows():
        # We add the URIs of concepts that should have the skos:related relationship with each other.
        related_uris = []

        # Cover class concepts
        cover_class_concept_uri = 'cover-class-' + str(row[0])
        g.add((URIRef('cover-class'), SKOS.member, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(cover_class_concept_uri), RDFS.label, Literal(row[2])))
        g.add((URIRef(cover_class_concept_uri), SKOS.prefLabel, Literal(row[2])))
        g.add((URIRef(cover_class_concept_uri), SKOS.definition, Literal(row[1])))
        g.add((URIRef(cover_class_concept_uri), DCTERMS.description, Literal(row[1])))
        g.add((URIRef(cover_class_concept_uri), DCTERMS.identifier, Literal('http://anzsoil.org/def/au/asls/vegetation/' + cover_class_concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(cover_class_concept_uri), UI.isMemberOf, URIRef('cover-class')))
        g.add((URIRef(cover_class_concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.notation, Literal(row[0])))

        # Crown separation ratio concepts
        concept_uri = 'crown-separation-ratio-' + str(i) # TODO: review this...
        g.add((URIRef('crown-separation-ratio'), SKOS.member, URIRef(concept_uri)))
        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[3])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[3])))
        g.add((URIRef(concept_uri), DCTERMS.identifier,
               Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('crown-separation-ratio')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(i))) # TODO: review this...
        g.add((URIRef(concept_uri), SKOS.narrower, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.broader, URIRef(concept_uri)))
        related_uris.append(concept_uri)

        # Crown cover concepts
        concept_uri = 'crown-cover-' + str(i)  # TODO: review this...
        g.add((URIRef('crown-cover'), SKOS.member, URIRef(concept_uri)))
        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[4])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[4])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[4])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[4])))
        g.add((URIRef(concept_uri), DCTERMS.identifier,
               Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('crown-cover')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(i)))  # TODO: review this...
        g.add((URIRef(concept_uri), SKOS.narrower, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.broader, URIRef(concept_uri)))
        related_uris.append(concept_uri)

        # Foliage cover concepts
        concept_uri = 'foliage-cover-' + str(i)  # TODO: review this...
        g.add((URIRef('foliage-cover'), SKOS.member, URIRef(concept_uri)))
        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[5])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[5])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[5])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[5])))
        g.add((URIRef(concept_uri), DCTERMS.identifier,
               Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('foliage-cover')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(i)))  # TODO: review this...
        g.add((URIRef(concept_uri), SKOS.narrower, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.broader, URIRef(concept_uri)))
        related_uris.append(concept_uri)

        # Add skos:related relationships for all concepts in the list `related_uris`.
        for x in range(len(related_uris)):
            for y in range(x + 1, len(related_uris)):
                g.add((URIRef(related_uris[x]), SKOS.related, URIRef(related_uris[y])))
                g.add((URIRef(related_uris[y]), SKOS.related, URIRef(related_uris[x])))

    g.serialize('cover-class-content.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('cover-class.xlsx')

    create_register_rdf()
    create_content(df)