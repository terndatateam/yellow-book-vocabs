import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies
from rdf_utils.namespace import REG, UI, SKOS_EXT, SOSA


def create_content(df):
    g = Graph()

    bind_curies(g)

    cover_class_collection_uri = 'http://linked.data.gov.au/def/tern-cv/f0b4061e-a73a-4b81-ba76-7df5e4f6fd31'

    # Cover class collection
    g.add((URIRef(cover_class_collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(cover_class_collection_uri), DCTERMS.source, Literal('Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef(cover_class_collection_uri), SKOS.definition, Literal(definition)))

    csr_collection_uri = 'http://linked.data.gov.au/def/tern-cv/988e5fa2-b392-4f69-888a-2c6415c0e5c8'

    # Crown separation ratio collection
    g.add((URIRef(csr_collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(csr_collection_uri), DCTERMS.source, Literal(
        'Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef(csr_collection_uri), SKOS.definition, Literal(definition)))

    crown_cover_collection_uri = 'http://linked.data.gov.au/def/tern-cv/30694ad0-864e-41ae-9fff-b35463c9c930'

    # Crown cover collection
    g.add((URIRef(crown_cover_collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(crown_cover_collection_uri), DCTERMS.source, Literal(
        'Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef(crown_cover_collection_uri), SKOS.definition, Literal(definition)))

    foliage_cover_collection_uri = 'http://linked.data.gov.au/def/tern-cv/397f11af-21bb-400c-844f-35a4c02a4b15'

    # Foliage cover collection
    g.add((URIRef(foliage_cover_collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(foliage_cover_collection_uri), DCTERMS.source, Literal(
        'Page 80 to 83 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'The recorded cover values collected using the methods below can be converted to cover classes as shown in Table 17. Crown cover classes are those used in Walker and Hopkins (1990) and these were selected to coincide as closely as possible with the cover classes of previous classifications (e.g. the projective foliage cover classes of Specht et al. 1974).'
    g.add((URIRef(foliage_cover_collection_uri), SKOS.definition, Literal(definition)))

    for i, row in df.iterrows():
        # We add the URIs of concepts that should have the skos:related relationship with each other.
        related_uris = []

        # Cover class concepts
        cover_class_concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[6]
        g.add((URIRef(cover_class_collection_uri), SKOS.member, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(cover_class_concept_uri), SKOS.prefLabel, Literal(row[2])))
        g.add((URIRef(cover_class_concept_uri), SKOS.definition, Literal(row[1])))
        g.add((URIRef(cover_class_concept_uri), SKOS.notation, Literal(row[0])))

        # Crown separation ratio concepts
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[7]
        g.add((URIRef(csr_collection_uri), SKOS.member, URIRef(concept_uri)))
        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(i)))
        g.add((URIRef(concept_uri), SKOS.narrower, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.broader, URIRef(concept_uri)))
        related_uris.append(concept_uri)

        # Crown cover concepts
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[8]
        g.add((URIRef(crown_cover_collection_uri), SKOS.member, URIRef(concept_uri)))
        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[4])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[4])))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(i)))
        g.add((URIRef(concept_uri), SKOS.narrower, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.broader, URIRef(concept_uri)))
        related_uris.append(concept_uri)

        # Foliage cover concepts
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[9]
        g.add((URIRef(foliage_cover_collection_uri), SKOS.member, URIRef(concept_uri)))
        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[5])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[5])))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(i)))
        g.add((URIRef(concept_uri), SKOS.narrower, URIRef(cover_class_concept_uri)))
        g.add((URIRef(cover_class_concept_uri), SKOS.broader, URIRef(concept_uri)))
        related_uris.append(concept_uri)

        # Add skos:related relationships for all concepts in the list `related_uris`.
        for x in range(len(related_uris)):
            for y in range(x + 1, len(related_uris)):
                g.add((URIRef(related_uris[x]), SKOS.related, URIRef(related_uris[y])))
                g.add((URIRef(related_uris[y]), SKOS.related, URIRef(related_uris[x])))

    g.serialize('cover-class-tern.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('cover-class.xlsx')

    create_content(df)