@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix reg: <http://purl.org/linked-data/registry#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix sosa: <http://www.w3.org/ns/sosa/> .
@prefix ui: <http://purl.org/linked-data/registry-ui#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<vegetation> a reg:Register ;
    rdfs:label "Vegetation" ;
    dct:contributor <https://orcid.org/0000-0002-3884-3420>,
        <https://orcid.org/0000-0002-3903-254X> ;
    dct:created "2020-06-19"^^xsd:date ;
    dct:creator <https://orcid.org/0000-0002-6047-9864> ;
    dct:description """<p>This register contains a machine-readable representation of the classifiers described in chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
<p>This chapter identifies which vegetation attributes should be measured and recorded at field sites in order to describe and classify Australian vegetation.7 The attributes chosen – as well as the required level of detail – depend on the purpose of the survey, which needs to be explicitly recorded.</p>
<p>The data was converted from the print representation to this linked-data form by <a href='https://orcid.org/0000-0002-6047-9864'>Edmond Chuc</a> assisted by <a href='https://orcid.org/0000-0002-3884-3420'>Simon J D Cox</a>."""^^rdf:HTML ;
    dct:isFormatOf "<p>Chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a></p>"^^rdf:HTML ;
    dct:license <http://creativecommons.org/licences/by/4.0> ;
    dct:modified "2020-06-23"^^xsd:date ;
    dct:publisher <http://www.publish.csiro.au/> ;
    dct:rights "copyright CSIRO 2009, 2020. All rights reserved." ;
    dct:source "National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)" ;
    reg:notation "vegetation" ;
    ui:hierarchyChildProperty skos:member ;
    ui:hierarchyRootProperty <http://linked.data.gov.au/def/skos-ext#topMemberOf> ;
    rdfs:seeAlso <http://www.publish.csiro.au/nid/22/pid/5230.htm> ;
    owl:imports dc:,
        <http://purl.org/linked-data/registry>,
        ui:,
        sosa: .

