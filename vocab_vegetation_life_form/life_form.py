import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies
from rdf_utils.namespace import REG, UI, SKOS_EXT, SOSA

import datetime


def create_register_rdf():
    g = Graph()

    bind_curies(g)

    g.add((URIRef('vegetation'), RDF.type, REG.Register))
    g.add((URIRef('vegetation'), RDFS.label, Literal('Vegetation')))
    g.add((URIRef('vegetation'), DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3884-3420')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3903-254X')))
    g.add((URIRef('vegetation'), DCTERMS.created, Literal('2020-06-19', datatype=XSD.date)))
    g.add((URIRef('vegetation'), DCTERMS.description, Literal("""<p>This register contains a machine-readable representation of the classifiers described in chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
<p>This chapter identifies which vegetation attributes should be measured and recorded at field sites in order to describe and classify Australian vegetation.7 The attributes chosen – as well as the required level of detail – depend on the purpose of the survey, which needs to be explicitly recorded.</p>
<p>The data was converted from the print representation to this linked-data form by <a href='https://orcid.org/0000-0002-6047-9864'>Edmond Chuc</a> assisted by <a href='https://orcid.org/0000-0002-3884-3420'>Simon J D Cox</a>.""", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.isFormatOf, Literal("<p>Chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a></p>", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.license, URIRef('http://creativecommons.org/licences/by/4.0')))
    g.add((URIRef('vegetation'), DCTERMS.modified, Literal(datetime.date.today())))
    g.add((URIRef('vegetation'), DCTERMS.publisher, URIRef('http://www.publish.csiro.au/')))
    g.add((URIRef('vegetation'), DCTERMS.rights, Literal('copyright CSIRO 2009, 2020. All rights reserved.')))
    g.add((URIRef('vegetation'), DCTERMS.source, Literal("National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)")))
    g.add((URIRef('vegetation'), REG.notation, Literal('vegetation')))
    g.add((URIRef('vegetation'), UI.hierarchyChildProperty, SKOS.member))
    g.add((URIRef('vegetation'), UI.hierarchyRootProperty, SKOS_EXT.topMemberOf))
    g.add((URIRef('vegetation'), RDFS.seeAlso, URIRef('http://www.publish.csiro.au/nid/22/pid/5230.htm')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(DC)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef('http://purl.org/linked-data/registry')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(UI)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(SOSA)))

    g.serialize('life-form-register.ttl', format='turtle')


def create_content(df):
    g = Graph()

    bind_curies(g)

    g.add((URIRef('life-form'), RDF.type, SKOS.Collection))
    g.add((URIRef('life-form'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('life-form'), SKOS.prefLabel, Literal('Life form')))
    g.add((URIRef('life-form'), DCTERMS.source, Literal('Page 80 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'Life form describes what a plant looks like. At the most general level of classification, formations, there are only two life forms: woody plants (w) and non-woody plants (nw). Woody plants include all trees, palms, arborescent cycads, tree ferns, xanthorrhoeas, shrubs and woody vines. All other plants with little or no woody tissue are classified as non-woody including annuals, grasses, grass-like plants, forbs, crusts, bryophytes and algae.'
    g.add((URIRef('life-form'), SKOS.definition, Literal(definition)))
    g.add((URIRef('life-form'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('life-form'), UI.topMemberOf, URIRef('')))

    for i, row in df.iterrows():
        concept_uri = 'life-form-' + str(row[0])

        g.add((URIRef('life-form'), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[1])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[1])))
        g.add((URIRef(concept_uri), DCTERMS.identifier, Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('life-form')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(row[0])))

    g.serialize('life-form-content.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('life-form.xlsx')

    create_register_rdf()
    create_content(df)