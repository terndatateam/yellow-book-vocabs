import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies
from rdf_utils.namespace import REG, UI, SKOS_EXT, SOSA

import datetime


def create_register_rdf():
    g = Graph()

    bind_curies(g)

    g.add((URIRef('vegetation'), RDF.type, REG.Register))
    g.add((URIRef('vegetation'), RDFS.label, Literal('Vegetation')))
    g.add((URIRef('vegetation'), DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3884-3420')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3903-254X')))
    g.add((URIRef('vegetation'), DCTERMS.created, Literal('2020-06-19', datatype=XSD.date)))
    g.add((URIRef('vegetation'), DCTERMS.description, Literal("""<p>This register contains a machine-readable representation of the classifiers described in chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
<p>This chapter identifies which vegetation attributes should be measured and recorded at field sites in order to describe and classify Australian vegetation.7 The attributes chosen – as well as the required level of detail – depend on the purpose of the survey, which needs to be explicitly recorded.</p>
<p>The data was converted from the print representation to this linked-data form by <a href='https://orcid.org/0000-0002-6047-9864'>Edmond Chuc</a> assisted by <a href='https://orcid.org/0000-0002-3884-3420'>Simon J D Cox</a>.""", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.isFormatOf, Literal("<p>Chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a></p>", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.license, URIRef('http://creativecommons.org/licences/by/4.0')))
    g.add((URIRef('vegetation'), DCTERMS.modified, Literal(datetime.date.today())))
    g.add((URIRef('vegetation'), DCTERMS.publisher, URIRef('http://www.publish.csiro.au/')))
    g.add((URIRef('vegetation'), DCTERMS.rights, Literal('copyright CSIRO 2009, 2020. All rights reserved.')))
    g.add((URIRef('vegetation'), DCTERMS.source, Literal("National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)")))
    g.add((URIRef('vegetation'), REG.notation, Literal('vegetation')))
    g.add((URIRef('vegetation'), UI.hierarchyChildProperty, SKOS.member))
    g.add((URIRef('vegetation'), UI.hierarchyRootProperty, SKOS_EXT.topMemberOf))
    g.add((URIRef('vegetation'), RDFS.seeAlso, URIRef('http://www.publish.csiro.au/nid/22/pid/5230.htm')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(DC)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef('http://purl.org/linked-data/registry')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(UI)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(SOSA)))

    g.serialize('rainforest-complexity-register.ttl', format='turtle')


def create_content(df):
    g = Graph()

    bind_curies(g)

    g.add((URIRef('rainforest-complexity'), RDF.type, SKOS.Collection))
    g.add((URIRef('rainforest-complexity'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('rainforest-complexity'), SKOS.prefLabel, Literal('Rainforest complexity')))
    g.add((URIRef('rainforest-complexity'), DCTERMS.source, Literal('Page 109 to 111 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'Tropical and subtropical rainforests of eastern Australia are classified as simple, simple–complex or complex depending on their structural complexity.'
    g.add((URIRef('rainforest-complexity'), SKOS.definition, Literal(definition)))
    g.add((URIRef('rainforest-complexity'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('rainforest-complexity'), UI.topMemberOf, URIRef('')))

    for i, row in df.iterrows():
        concept_uri = 'rainforest-complexity-' + str(row[0])

        g.add((URIRef('rainforest-complexity'), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[2])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[2])))
        g.add((URIRef(concept_uri), DCTERMS.identifier, Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('rainforest-complexity')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(row[0])))

    g.serialize('rainforest-complexity-content.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('rainforest-complexity.xlsx')

    create_register_rdf()
    create_content(df)