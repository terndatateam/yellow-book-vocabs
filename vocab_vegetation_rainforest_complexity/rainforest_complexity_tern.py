import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies


def create_content(df):
    g = Graph()

    bind_curies(g)

    collection_uri = 'http://linked.data.gov.au/def/tern-cv/50a88e8d-d79f-4b91-b499-e55e193e3d1a'

    g.add((URIRef(collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(collection_uri), DCTERMS.source, Literal('Page 109 to 111 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'Tropical and subtropical rainforests of eastern Australia are classified as simple, simple–complex or complex depending on their structural complexity.'
    g.add((URIRef(collection_uri), SKOS.definition, Literal(definition)))

    for i, row in df.iterrows():
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[3]

        g.add((URIRef(collection_uri), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[2])))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(row[0])))

    g.serialize('rainforest-complexity-tern.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('rainforest-complexity.xlsx')

    create_content(df)