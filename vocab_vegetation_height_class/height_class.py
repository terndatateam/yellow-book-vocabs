import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies
from rdf_utils.namespace import REG, UI, SKOS_EXT, SOSA

import datetime


def create_register_rdf():
    g = Graph()

    bind_curies(g)

    g.add((URIRef('vegetation'), RDF.type, REG.Register))
    g.add((URIRef('vegetation'), RDFS.label, Literal('Vegetation')))
    g.add((URIRef('vegetation'), DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3884-3420')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3903-254X')))
    g.add((URIRef('vegetation'), DCTERMS.created, Literal('2020-06-19', datatype=XSD.date)))
    g.add((URIRef('vegetation'), DCTERMS.description, Literal("""<p>This register contains a machine-readable representation of the classifiers described in chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
<p>This chapter identifies which vegetation attributes should be measured and recorded at field sites in order to describe and classify Australian vegetation.7 The attributes chosen – as well as the required level of detail – depend on the purpose of the survey, which needs to be explicitly recorded.</p>
<p>The data was converted from the print representation to this linked-data form by <a href='https://orcid.org/0000-0002-6047-9864'>Edmond Chuc</a> assisted by <a href='https://orcid.org/0000-0002-3884-3420'>Simon J D Cox</a>.""", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.isFormatOf, Literal("<p>Chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a></p>", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.license, URIRef('http://creativecommons.org/licences/by/4.0')))
    g.add((URIRef('vegetation'), DCTERMS.modified, Literal(datetime.date.today())))
    g.add((URIRef('vegetation'), DCTERMS.publisher, URIRef('http://www.publish.csiro.au/')))
    g.add((URIRef('vegetation'), DCTERMS.rights, Literal('copyright CSIRO 2009, 2020. All rights reserved.')))
    g.add((URIRef('vegetation'), DCTERMS.source, Literal("National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)")))
    g.add((URIRef('vegetation'), REG.notation, Literal('vegetation')))
    g.add((URIRef('vegetation'), UI.hierarchyChildProperty, SKOS.member))
    g.add((URIRef('vegetation'), UI.hierarchyRootProperty, SKOS_EXT.topMemberOf))
    g.add((URIRef('vegetation'), RDFS.seeAlso, URIRef('http://www.publish.csiro.au/nid/22/pid/5230.htm')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(DC)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef('http://purl.org/linked-data/registry')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(UI)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(SOSA)))

    g.serialize('height-class-register.ttl', format='turtle')


def create_content(df):
    g = Graph()

    bind_curies(g)

    g.add((URIRef('height-class'), RDF.type, SKOS.Collection))
    g.add((URIRef('height-class'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('height-class'), SKOS.prefLabel, Literal('Height class')))
    g.add((URIRef('height-class'), DCTERMS.source, Literal('Page 93 to 95 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = """In the field, height should be measured, rather than the height class estimated. Height can be measured using measuring tapes or poles for low vegetation. Clinometers, laser or sonic ranging instruments, visual sighting instruments or LIDAR can be used for tall vegetation (see Brack 1998; Abed and Stephens 2003). Inaccuracy in measurements increases as crown closure and height increases.
Record the height from the ground to the highest part of the plant above ground. Where the height of flower stalks (e.g. in grasses, grass trees) or leaves(e.g. in palms, cycads, grass trees, tree ferns) add significantly to plant height and contribute significantly to a stratum, then record two measurements: total height from ground level to the top of the highest part of the plant, and height from ground level to the top of the leaves (e.g. Xanthorrhoea johnsonii 2.5 m/1.3 m; Sorghum intrans 1.9 m/1.3 m). This provides an accurate record and allows various uses in analysis.
The recorded heights can then be converted to height classes as shown in Table 20."""
    g.add((URIRef('height-class'), SKOS.definition, Literal(definition)))
    g.add((URIRef('height-class'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('height-class'), UI.topMemberOf, URIRef('')))

    for i, row in df.iterrows():
        concept_uri = 'height-class-' + str(row[0])

        g.add((URIRef('height-class'), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[1])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[1])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[1])))
        g.add((URIRef(concept_uri), DCTERMS.identifier, Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('height-class')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        g.add((URIRef(concept_uri), SKOS.notation, Literal(row[0])))
        if not pd.isnull(row[2]):
            g.add((URIRef(concept_uri), SKOS.altLabel, Literal(row[2])))
        if not pd.isnull(row[3]):
            g.add((URIRef(concept_uri), SKOS.altLabel, Literal(row[3])))

    g.serialize('height-class-content.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('height-class.xlsx')

    create_register_rdf()
    create_content(df)