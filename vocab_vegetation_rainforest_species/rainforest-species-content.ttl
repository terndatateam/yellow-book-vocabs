@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix sosa: <http://www.w3.org/ns/sosa/> .
@prefix ui: <http://purl.org/linked-data/registry-ui#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<rainforest-species-M> a skos:Concept ;
    rdfs:label "Mixed" ;
    dct:description "No one or two species combined make 50% or more of the crown cover in the tallest stratum." ;
    dct:identifier "http://anzsoil.org/def/au/asls/vegetation/rainforest-species-M"^^xsd:anyURI ;
    ui:isMemberOf <rainforest-species> ;
    owl:sameAs <http://anzsoil.org/def/au/asls/vegetation/rainforest-species-M> ;
    skos:definition "No one or two species combined make 50% or more of the crown cover in the tallest stratum." ;
    skos:notation "M" ;
    skos:prefLabel "Mixed" .

<rainforest-species-S> a skos:Concept ;
    rdfs:label "One or two species description" ;
    dct:description "The one or two species described constitute 50% or more of the crown cover of the tallest stratum. Common, generic or specific names can be used (e.g. coachwood-crabapple; Ceratopetalum-Schizomeria; Ceratopetalum apetalum-Schizomeria ovata). Use species name abbreviations for coding (e.g. CEAPE.SCOVA) except for fan or feather palms, which should not be used as common names because they are used to denote structural features (see 'Indicator growth forms'). Include sclerophyll species if they constitute 50% or more of the crown cover of the tallest stratum." ;
    dct:identifier "http://anzsoil.org/def/au/asls/vegetation/rainforest-species-S"^^xsd:anyURI ;
    ui:isMemberOf <rainforest-species> ;
    owl:sameAs <http://anzsoil.org/def/au/asls/vegetation/rainforest-species-S> ;
    skos:definition "The one or two species described constitute 50% or more of the crown cover of the tallest stratum. Common, generic or specific names can be used (e.g. coachwood-crabapple; Ceratopetalum-Schizomeria; Ceratopetalum apetalum-Schizomeria ovata). Use species name abbreviations for coding (e.g. CEAPE.SCOVA) except for fan or feather palms, which should not be used as common names because they are used to denote structural features (see 'Indicator growth forms'). Include sclerophyll species if they constitute 50% or more of the crown cover of the tallest stratum." ;
    skos:notation "S" ;
    skos:prefLabel "One or two species description" .

<rainforest-species-X> a skos:Concept ;
    rdfs:label "Mixed plus one species" ;
    dct:description "Although no species or two species combined make up 50% of the crown cover of the dominant stratum, one species (the one species nominated) is conspicuously abundant. As above, common, generic or specific names can be used except for feather or fan palms (e.g. Mxed booyong; Mixed Argyrodendron; Mixed Argyrodendron trifoliolatum or ARTRI). This floristic term can be used to nominatespecies of particular indicator value to the user." ;
    dct:identifier "http://anzsoil.org/def/au/asls/vegetation/rainforest-species-X"^^xsd:anyURI ;
    ui:isMemberOf <rainforest-species> ;
    owl:sameAs <http://anzsoil.org/def/au/asls/vegetation/rainforest-species-X> ;
    skos:definition "Although no species or two species combined make up 50% of the crown cover of the dominant stratum, one species (the one species nominated) is conspicuously abundant. As above, common, generic or specific names can be used except for feather or fan palms (e.g. Mxed booyong; Mixed Argyrodendron; Mixed Argyrodendron trifoliolatum or ARTRI). This floristic term can be used to nominatespecies of particular indicator value to the user." ;
    skos:notation "X" ;
    skos:prefLabel "Mixed plus one species" .

<rainforest-species> a skos:Collection,
        sosa:ObservableProperty ;
    dct:description """The type of rainforest is named after the most abundant species of the dominant stratum, using the following system.
    
Many rainforest species occur in clusters of five or six trees. Care should be taken to ensure that the species of the tallest stratum are found over a much wider area than a few trees if the species qualifications are used.""" ;
    dct:source "Page 113 to 114 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing." ;
    ui:topMemberOf <> ;
    skos:definition """The type of rainforest is named after the most abundant species of the dominant stratum, using the following system.
    
Many rainforest species occur in clusters of five or six trees. Care should be taken to ensure that the species of the tallest stratum are found over a much wider area than a few trees if the species qualifications are used.""" ;
    skos:member <rainforest-species-M>,
        <rainforest-species-S>,
        <rainforest-species-X> ;
    skos:prefLabel "Rainforest species" .

