import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, SKOS

from rdf_utils import bind_curies


# TODO: Review the local name of Cryptogam and Food. The local name is usually constructed from the chapter
#  (growth-form) and the red code. Both Cryptogam and Food do not have codes assigned.

# TODO: Review the local name of "Aquatic higher plants" with code a1.0 (or w). Because it has whitespace, I have
#  URL-encoded the local name to growth-form-a1.0+%28or+w%29. It is a bit ugly. Is there a better way?

# TODO: Review the recording of the height range information. It is currently recorded using the skos:hiddenLabel
#  property. Should we create a new property to record this? Should we use the DATA ontology and record it as a
#  data:QuantitativeRange?


def create_content(df):
    g = Graph()

    bind_curies(g)

    collection_uri = 'http://linked.data.gov.au/def/tern-cv/32ef7761-e46d-41bc-aab8-39d3ddf9caa4'

    g.add((URIRef(collection_uri), RDF.type, SKOS.Collection))
    g.add((URIRef(collection_uri), DCTERMS.source, Literal('Page 88 to 93 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'At Level 1, only two life forms exist (woody plants or non-woody plants). In contrast, at Level 2, many more detailed categories of growth forms are used. The term growth form is used in a broad sense to describe the form or shape of individual plants (e.g. tree, shrub) or Australian broad floristic land cover types (e.g. native vegetation such as mallee, chenopod shrub; or non-native vegetation such as wheat fields, orchards). The following glossary (modified from ESCAVI 2003 and alphabetised by growth form name) defines the growth forms for structural formations.'
    g.add((URIRef(collection_uri), SKOS.definition, Literal(definition)))

    for i, row in df.iterrows():
        concept_uri = 'http://linked.data.gov.au/def/tern-cv/' + row[5]

        g.add((URIRef(collection_uri), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[0])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[2])))
        if not pd.isnull(row[1]):
            g.add((URIRef(concept_uri), SKOS.notation, Literal(row[1])))
        if not pd.isnull(row[3]):
            g.add((URIRef(concept_uri), SKOS.hiddenLabel, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.hiddenLabel, Literal(row[4])))

    g.serialize('growth-form-tern.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('growth-form.xlsx')

    create_content(df)