import pandas as pd
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDF, RDFS, DCTERMS, XSD, SKOS, OWL, DC

from rdf_utils import bind_curies
from rdf_utils.namespace import REG, UI, SKOS_EXT, SOSA

import datetime
from urllib.parse import quote_plus


# TODO: Review the local name of Cryptogam and Food. The local name is usually constructed from the chapter
#  (growth-form) and the red code. Both Cryptogam and Food do not have codes assigned.

# TODO: Review the local name of "Aquatic higher plants" with code a1.0 (or w). Because it has whitespace, I have
#  URL-encoded the local name to growth-form-a1.0+%28or+w%29. It is a bit ugly. Is there a better way?

# TODO: Review the recording of the height range information. It is currently recorded using the skos:hiddenLabel
#  property. Should we create a new property to record this? Should we use the DATA ontology and record it as a
#  data:QuantitativeRange?


def create_register_rdf():
    g = Graph()

    bind_curies(g)

    g.add((URIRef('vegetation'), RDF.type, REG.Register))
    g.add((URIRef('vegetation'), RDFS.label, Literal('Vegetation')))
    g.add((URIRef('vegetation'), DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3884-3420')))
    g.add((URIRef('vegetation'), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3903-254X')))
    g.add((URIRef('vegetation'), DCTERMS.created, Literal('2020-06-19', datatype=XSD.date)))
    g.add((URIRef('vegetation'), DCTERMS.description, Literal("""<p>This register contains a machine-readable representation of the classifiers described in chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
<p>This chapter identifies which vegetation attributes should be measured and recorded at field sites in order to describe and classify Australian vegetation.7 The attributes chosen – as well as the required level of detail – depend on the purpose of the survey, which needs to be explicitly recorded.</p>
<p>The data was converted from the print representation to this linked-data form by <a href='https://orcid.org/0000-0002-6047-9864'>Edmond Chuc</a> assisted by <a href='https://orcid.org/0000-0002-3884-3420'>Simon J D Cox</a>.""", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.isFormatOf, Literal("<p>Chapter 6 <i>Vegetation</i>, by R.J. Hnatiuk, R. Thackway and J. Walker, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a></p>", datatype=RDF.HTML)))
    g.add((URIRef('vegetation'), DCTERMS.license, URIRef('http://creativecommons.org/licences/by/4.0')))
    g.add((URIRef('vegetation'), DCTERMS.modified, Literal(datetime.date.today())))
    g.add((URIRef('vegetation'), DCTERMS.publisher, URIRef('http://www.publish.csiro.au/')))
    g.add((URIRef('vegetation'), DCTERMS.rights, Literal('copyright CSIRO 2009, 2020. All rights reserved.')))
    g.add((URIRef('vegetation'), DCTERMS.source, Literal("National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)")))
    g.add((URIRef('vegetation'), REG.notation, Literal('vegetation')))
    g.add((URIRef('vegetation'), UI.hierarchyChildProperty, SKOS.member))
    g.add((URIRef('vegetation'), UI.hierarchyRootProperty, SKOS_EXT.topMemberOf))
    g.add((URIRef('vegetation'), RDFS.seeAlso, URIRef('http://www.publish.csiro.au/nid/22/pid/5230.htm')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(DC)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef('http://purl.org/linked-data/registry')))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(UI)))
    g.add((URIRef('vegetation'), OWL.imports, URIRef(SOSA)))

    g.serialize('growth-form-register.ttl', format='turtle')


def create_content(df):
    g = Graph()

    bind_curies(g)

    g.add((URIRef('growth-form'), RDF.type, SKOS.Collection))
    g.add((URIRef('growth-form'), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef('growth-form'), SKOS.prefLabel, Literal('Growth form')))
    g.add((URIRef('growth-form'), DCTERMS.source, Literal('Page 88 to 93 of the National Committee on Soil Terrain, National Committee on Soil Terrain, CSIRO Publishing, & Ebooks Corporation. (2009). Australian soil and land survey : Field handbook (3rd ed., Australian soil and land survey handbook; v. 1). Collingwood, Vic.: CSIRO Publishing.')))
    definition = 'At Level 1, only two life forms exist (woody plants or non-woody plants). In contrast, at Level 2, many more detailed categories of growth forms are used. The term growth form is used in a broad sense to describe the form or shape of individual plants (e.g. tree, shrub) or Australian broad floristic land cover types (e.g. native vegetation such as mallee, chenopod shrub; or non-native vegetation such as wheat fields, orchards). The following glossary (modified from ESCAVI 2003 and alphabetised by growth form name) defines the growth forms for structural formations.'
    g.add((URIRef('growth-form'), SKOS.definition, Literal(definition)))
    g.add((URIRef('growth-form'), DCTERMS.description, Literal(definition)))
    g.add((URIRef('growth-form'), UI.topMemberOf, URIRef('')))

    for i, row in df.iterrows():
        if not pd.isnull(row[1]):
            concept_uri = 'growth-form-' + quote_plus(str(row[1]))
        else:
            concept_uri = 'growth-form-' + str(row[0])

        g.add((URIRef('growth-form'), SKOS.member, URIRef(concept_uri)))

        g.add((URIRef(concept_uri), RDF.type, SKOS.Concept))
        g.add((URIRef(concept_uri), RDFS.label, Literal(row[0])))
        g.add((URIRef(concept_uri), SKOS.prefLabel, Literal(row[0])))
        g.add((URIRef(concept_uri), SKOS.definition, Literal(row[2])))
        g.add((URIRef(concept_uri), DCTERMS.description, Literal(row[2])))
        g.add((URIRef(concept_uri), DCTERMS.identifier, Literal('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri, datatype=XSD.anyURI)))
        g.add((URIRef(concept_uri), UI.isMemberOf, URIRef('growth-form')))
        g.add((URIRef(concept_uri), OWL.sameAs, URIRef('http://anzsoil.org/def/au/asls/vegetation/' + concept_uri)))
        if not pd.isnull(row[1]):
            g.add((URIRef(concept_uri), SKOS.notation, Literal(row[1])))
        if not pd.isnull(row[3]):
            g.add((URIRef(concept_uri), SKOS.hiddenLabel, Literal(row[3])))
        g.add((URIRef(concept_uri), SKOS.hiddenLabel, Literal(row[4])))

    g.serialize('growth-form-content.ttl', format='turtle')


if __name__ == '__main__':
    df = pd.read_excel('growth-form.xlsx')

    create_register_rdf()
    create_content(df)